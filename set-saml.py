#!/usr/bin/python2.7
# -*- coding: UTF-8 -*-

import os
import os.path
import re
import univention.udm
from univention.udm import UDM
from univention.config_registry.backend import ConfigRegistry
from pprint import pprint
from PyInquirer import prompt, Separator
from examples import custom_style_1, custom_style_2, custom_style_3

ucr         = ConfigRegistry()
ucr.load()


def getSchools():
  udm = UDM.admin().version(1).get('container/ou')
  udmOUs = udm.search(base=ucr['ldap/base'])
  schoolList = [
    {
      'type': 'checkbox',
      'message': 'Wähle Schulen aus',
      'name': 'schools',
      'choices': [ 
      ],
      'validate': lambda answer: 'You must choose at least one ' \
        if len(answer) == 0 else True
    }
  ]
  isSchool = False
  for ou in udmOUs:
    for role in ou.props.ucsschoolRole:
      if re.match('school:school:.*',role):
        isSchool = True
    if(isSchool):
      schoolVal = {
        "name" : ou.props.displayName,
        "value": ou.dn
      }
      schoolList[0]["choices"].append(schoolVal)
    isSchool = False
  return(schoolList)

def getSamlSp():
  udm = UDM.admin().version(1).get('saml/serviceprovider')
  udmSaml = udm.search(base=ucr['ldap/base'])
  samlList = [
    {
      'type': 'checkbox',
      'message': 'Wähle SAML Serviceprovider aus',
      'name': 'samlSp',
      'choices': [ 
      ],
      'validate': lambda answer: 'You must choose at least one.' \
        if len(answer) == 0 else True
    }
  ]
  for s in udmSaml:
    samlVal = {
        "name" : s.props.Identifier,
        "value": s.dn
      }
    samlList[0]["choices"].append(samlVal)
  return(samlList)

schoolList = getSchools()
samlList = getSamlSp()

schools = prompt(schoolList, style=custom_style_2)
samlSp = prompt(samlList, style=custom_style_2)

udm = UDM.admin().version(1).get('users/user')

for school in schools['schools']:
  ldap_base = "cn=users,{}".format(school)
  school_users = udm.search(base=ldap_base)

  changes = 0 

  for user in school_users:
    isset = False
    for s in samlSp['samlSp']:
      if s not in user.props.serviceprovider:
        if re.match("^gast\-.*$",user.props.username):
          print("User {} is guest, skip!".format(user.props.username))
        else:
          user.props.serviceprovider.append(s)
          user.save()
          changes = changes + 1
      
      
  print(school)
  print("  Durchgeführte Änderungen: {}".format(changes))
  print("")








